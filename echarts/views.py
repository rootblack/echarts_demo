# -*-coding:utf-8-*-
from __future__ import unicode_literals
from django.shortcuts import render
import json


def index(request):
    dataAxis = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh', 'aaa', 'bbb', 'ccc', 'ddd']
    data = [220, 182, 191, 234, 290, 330, 310, 123, 442, 321, 90, 149, 210, 122, 133, 334, 198, 123, 125, 220]
    yMax = 500;
    dataShadow = [' ']
    return render(request, 'index.html', 
                    {'dataAxis': json.dumps(dataAxis), 
                    'data' : json.dumps(data), 
                    'yMax' : json.dumps(yMax), 
                    'dataShadow' : json.dumps(dataShadow), 
                    })